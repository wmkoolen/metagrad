%% Implementation of Algorithm 2: Metagrad Slave

classdef Slave_full < handle
  properties
    eta;    % learning rate
    w;      % mean
    Sigma;  % covariance
    domain; % weights are always projected onto domain (U in paper)
  end


  methods
    function obj = Slave_full(eta, domain)
      d = domain.get_dimension();
      D = domain.get_full_diameter();

      obj.domain = domain;
      obj.eta = eta;

      obj.w = domain.get_center();  % mean
      obj.Sigma = D^2*eye(d);      % covariance

    end

    function update(obj, w_master, grad)


      %% update (unprojected) to \tilde w_{t+1}
      Sg = obj.Sigma*grad;
      Z  = 1+2*obj.eta^2*(grad'*Sg);

      obj.Sigma = obj.Sigma - 2*obj.eta^2/Z*(Sg*Sg');

      %% The paper prescribes w_tilde = obj.w - obj.eta * obj.Sigma * grad * (1 + 2*obj.eta * grad'*(obj.w - w_master))
      %% we plug in obj.Sigma * grad = Sg/Z
      
      w_tilde   = obj.w - obj.eta*(1+2*obj.eta*grad'*(obj.w-w_master))/Z*Sg;

      %% project mean onto domain
      obj.w = obj.domain.project_full(obj.Sigma, w_tilde);
    end
  end
end

