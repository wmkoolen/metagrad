classdef Ball < handle
  properties
    d; % dimension
    r; % radius
  end

  methods

    function obj = Ball(d, r)
      obj.d = d;
      obj.r = r;
    end

    function d = get_dimension(obj)
      d = obj.d;
    end

    function w = get_center(obj)
      w = zeros(obj.d, 1);
    end
  
      

    function D = get_full_diameter(obj)
      D = 2*obj.r;
    end

    function D = get_diag_diameter(obj)
      D = 2*obj.r;
    end
    

    %% returns argmin_{w in ball} (w-wtilde) Sigma^{-1} (w-wtilde)
    function w = project_full(obj, Sigma, wtilde)

      if wtilde'*wtilde <= obj.r^2
	w = wtilde;
	return;
      end
      
      [U, D] = eig(Sigma);  % Sigma = U*D*U'
      
      %% convert input to diagonal form, solve, convert output back
      w = U * obj.project_diag(diag(D), U'*wtilde);
      
      assert(abs(w'*w - obj.r^2) < 1e-7);    
    end

    function w = project_diag(obj, sigma, wtilde)
      if wtilde'*wtilde <= obj.r^2
	w = wtilde;
	return;
      end

      tol = 1e-7;

      %% starting point derived from solving Jensened constraint
      %% starts closer than lambda=0, but norm still too big
      lambda = (sqrt(wtilde'*wtilde)/obj.r-1)/(wtilde.^2'*sigma/(wtilde'*wtilde));
     
      while true
	assert(lambda >= 0);
	nsq = wtilde'.^2 * (1+lambda*sigma).^-2;
	if abs(nsq -obj.r^2) < tol
	  break;
	end
	lambda = lambda - (obj.r^2 - nsq)/(2* (wtilde.^2 .* sigma)' * (1+lambda*sigma).^-3);
      end
      
      w = wtilde ./ (1+lambda*sigma);

      assert(abs(w'*w -obj.r^2) < tol);    
    end
  end  
end
