%% MetaGrad example
%% uses absolute loss on two randomized outcomes


T = 1000; % number of steps
G = 1;    % gradient norm bound
d = 1;    % dimension
r = 1;    % domain is a ball of radius r
domain = Ball(d, r);


%% parameters of objective
% absolute loss on two randomised outcomes

outcomes = [-.5 .5];          % possible outcomes
alpha = .4;                   % probability of first outcome
x = rand(1, T) >= alpha;      % randomized outcome indices
sample = outcomes(1+x);

f = @(xhat, x) abs(xhat-x);   % loss function
g = @(xhat, x) sign(xhat-x);  % gradient

% start MetaGrad instance
[etas, pis] = MetaGrad_full.mkgrid(T, G, domain);
m = MetaGrad_full(etas, pis, domain);

lalg = nan(T, 1);              % array of losses

for t=1:T
  fprintf(2, 't = %d\r', t);
  
  w = m.get_weights();
  lalg(t) = f(w, sample(t));  % evaluate the loss of MetaGrad
  m.update(g(w, sample(t)));  % update with the gradient of the loss
end


%% evaluate objective function on grid for offline optimal loss
xs = linspace(-1, 1, 101);
[Tr, xsr] = meshgrid(1:T, xs);
fs = arrayfun(@(t, w) f(w, sample(t)), Tr, xsr);
Fstar = min(cumsum(fs, 2));


plot(1:T, cumsum(lalg)-Fstar');
xlabel('T');
ylabel('regret');