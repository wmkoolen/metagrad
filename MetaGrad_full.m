classdef MetaGrad_full < handle
  properties
    slaves;
    pis;     % slave weights.
    domain;  % weights are always projected onto domain (U in paper)
  end


  methods

    %% Constructor. Fires up a new metagrad instance.
    %% The typical way of getting etas and pis is via
    %% the MetaGrad_full.mkgrid static method.
    %% The canonical domain is the unit Euclidean ball.
    %% Use Ball(r,d) for a radius r ball in d dimensions.
    function obj = MetaGrad_full(etas, pis, domain)
      obj.slaves = Slave_full(etas(1), domain);
      for i = 2:length(etas)
          obj.slaves(i) = Slave_full(etas(i), domain);
      end
      obj.pis = pis;
      obj.domain = domain;
    end


    %% Compute the current metagrad weights vector
    function w = get_weights(obj)
      %% (tilted) master weights
      pslaves = [obj.slaves.eta] .* obj.pis;

      %% weighted average predictions of slaves
      w = [obj.slaves.w] * pslaves' / sum(pslaves);
    end


    %% Update the state using the gradient of the loss
    function update(obj, grad)

      %% ensure exp-concavity of surrogate losses
      if max([obj.slaves.eta]) * obj.domain.get_full_diameter() * norm(grad) > 1/5
	error('The maximum eta is too large for your gradient norms. This violates exp-concavity of the surrogate losses. Check your gradient norm bound (the G argument to mkgrid).');
      end
      

      w = obj.get_weights();
      
      for i = 1:length(obj.slaves)

	%% charge slave its surrogate loss of its mean
	r  = (w-obj.slaves(i).w)'*grad;
	eta = obj.slaves(i).eta;
        obj.pis(i) = obj.pis(i) * exp(eta*r - eta^2*r^2);

	%% and update slave
	obj.slaves(i).update(w, grad);
      end
      assert(all(obj.pis > eps)); % don't want underflow
    end
  end


  methods(Static)

    %% recommended MetaGrad grid as in equation (9)
    %%
    %% T: (bound on) number of rounds
    %% G: bound on l_2 norm of gradients
        
    function [etas, pi_etas] = mkgrid(T, G, domain)

      D = domain.get_full_diameter();

      netas = ceil(.5*log(T)/log(2));
      is = 0:netas;

      etas = 2.^-is/(5*D*G);
      pi_etas  = 1./((is+1).*(2+is));
    end
  end

end

