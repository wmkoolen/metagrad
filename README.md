# MetaGrad README #

This repository contains a first Matlab/Octave implementation of the MetaGrad algorithm from the paper

*
[MetaGrad: Multiple Learning Rates in Online Learning](http://papers.nips.cc/paper/6268-metagrad-multiple-learning-rates-in-online-learning.pdf)  
Tim van Erven and Wouter M. Koolen  
Advances in Neural Information Processing Systems (NIPS) 29, pages 3666–3674, December 2016.
*

It also contains an example script, [run_example.m](run_example.m), that runs MetaGrad on IID random data with absolute loss.



## Set up ##
1. Get the source code by cloning this git repository
2. Fire up octave or matlab
3. Execute 'run_example'. This will run MetaGrad and graph the regret.

If you want to try MetaGrad on your data, adapt [run_example.m](run_example.m).


## More information ##

The code currently implements full MetaGrad on the Euclidean ball. We are planning to implement the diagonal version and other domains (box, simplex, ...) at some point in the future. In the mean time any feedback or requests for features is very welcome.

Wouter and Tim  
[wmkoolen@cwi.nl](mailto:wmkoolen@cwi.nl)  
[tim@timvanerven.nl](mailto:tim@timvanerven.nl)